require("dotenv").config();
const util = require("util");
const { v4: uuidv4 } = require('uuid');
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    console.log("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  const res = (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
  return res;
};

// Implemented by Team JEMS: query random product row from Products table in MySQL
const queryRandomProduct = async () => {
  // Get all product rows
  const products = await queryAllProducts();

  // Get product at random index
  return products[Math.floor(Math.random()*products.length)];
};

// Implemented by Team JEMS: query all produts from Products table in MySQL
const queryAllProducts = async () => {
  const res = (await mysql.query("SELECT * FROM products"))[0]; // Return back the list of products
  return res;
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

// Implemented by Team JEMS: Inserts an order into the Orders table in MySQL
const insertOrder = async (order) => {
  // Generate random UUID V4
  const id = uuidv4();
  
  // Insert the order into the database
  const res = (await mysql.query("INSERT INTO orders (id, total_amount, user_id) VALUES (?, ?, ?)", [id, order.total_amount, order.user_id]))[0];
  
  // Insert each product in order.products into the table order_items to indicate relationship
  for (product in order.products) {
    await mysql.query("INSERT INTO order_items (id, order_id, product_id) VALUES (?, ?, ?)", [uuidv4(), id, product.product_id]);
  }
  return res;
};

// Implemented by Team JEMS: Updates a user in the Users table in MySQL
const updateUser = async (id, updates) => {
  // Use parameters in SQL query and return back the list
  return (await mysql.query("UPDATE users SET name = ?, email = ?, password = ? WHERE id = ?", [updates.name, updates.email, updates.password, id]))[0]; 
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
