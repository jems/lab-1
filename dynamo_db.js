require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const { v4: uuidv4 } = require('uuid');

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

// Implemented by Team JEMS: query random product from Product table in dynamoDB
const queryRandomProduct = async () => {
  // query all products
  const products = await queryAllProducts();

  // generate random index from all products
  const randomIdx = Math.floor(Math.random()*products.length);
  
  // get product at random index
  return products[randomIdx];
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

// Implemented by Team JEMS: query all products from Products table in dynamoDB
const queryAllProducts = async (category = "") => {
  // Create command to query all products
  const command = new ScanCommand({
    TableName: "Products",
  });

  // Await and parse response
  const response = await docClient.send(command);
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

// Implemented by Team JEMS: add order row to Orders table in dynamoDB
const insertOrder = async (order) => {
  // Create put command to add order
  const command = new PutCommand(
    {
      TableName: "Orders",
      Item: {
        Order: order,
        id: uuidv4(),
      },
    }
  );

  // Wait for response
  const response = await docClient.send(command);
  return response;
};

// Implemented by Team JEMS: update user row from Users table
const updateUser = async (id, updates) => {
  // Create update command
  const command = new UpdateCommand(
    {
      TableName: "Users",
      Key: {
        id: id
      },
      UpdateExpression: "set email = :email, #Name = :n, password = :password", // Sets new values
      ExpressionAttributeValues: {
        ":email": updates.email || "", // parameter for email attribute
        ":n": updates.name || "", // parameter for name attribute
        ":password": updates.password || "", // parameter for password attribute
      },
      ExpressionAttributeNames: {
        "#Name": "name" // Allows us to use keyword
      },
      ReturnValues: "ALL_OLD", // Returns ALL_OLD values
    }
  )

  // Wait for response and parse new attribute values
  const response = await docClient.send(command); // docClient sends command to database using DynamoDB API and then awaits response
  return response.Attributes;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
